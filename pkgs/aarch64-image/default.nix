{ stdenv, fetchurl, zstd, system }:

let
  base-path = system.config.system.build.sdImage;
  image-name = system.config.sdImage.imageName;
in
stdenv.mkDerivation {
  name = "aarch64-image";
  src = "${base-path}/sd-image/${image-name}.zst";
  preferLocalBuild = true;
  dontUnpack = true;
  dontBuild = true;
  dontConfigure = true;

  # Performance
  dontPatchELF = true;
  dontStrip = true;
  noAuditTmpdir = true;
  dontPatchShebangs = true;

  nativeBuildInputs = [
    zstd
  ];

  installPhase = ''
    zstdcat "$src" > $out
  '';
}
