{ nixpkgs, lib, kernel, extraModules ? [ ] }:

let
  baseModule = {
    nixpkgs.overlays = [
      (final: super: {
        zfs = super.zfs.overrideAttrs (_: {
          meta.platforms = [ ];
        });
      })
    ];

    time.timeZone = "UTC";

    boot.kernelPackages = kernel;

    system.stateVersion = "23.05";
  };

  buildModule = fdt: {
    boot.kernelParams = [ "console=ttyS2,1500000n8" "rootwait" "root=PARTLABEL=nixos" "rw" ];

    hardware.deviceTree.name = fdt;
  };

  buildSystem = module: nixpkgs.lib.nixosSystem {
    system = "aarch64-linux";

    modules = [
      "${nixpkgs}/nixos/modules/installer/sd-card/sd-image-aarch64.nix"
      module
      baseModule
      {
        networking.hostName = "nixos-aarch64";
      }
    ] ++ extraModules;
  };

  modules = {
    inherit baseModule buildModule;

    quartz64a = buildModule "rockchip/rk3566-quartz64-a.dtb";
    quartz64b = buildModule "rockchip/rk3566-quartz64-b.dtb";
    soquartz-model-a = buildModule "rockchip/rk3566-soquartz-model-a.dtb";
    soquartz-cm4 = buildModule "rockchip/rk3566-soquartz-cm4.dtb";
    soquartz-blade = buildModule "rockchip/rk3566-soquartz-blade.dtb";
    rock64 = buildModule "rockchip/rk3328-rock64.dtb";
    rockPro64 = buildModule "rockchip/rk3399-rockpro64.dtb";
    rockPro64v2 = buildModule "rockchip/rk3399-rockpro64-v2.dtb";
    roc-pc-rk3399 = buildModule "rockchip/rk3399-roc-pc.dtb";
    pinebookPro = buildModule "rockchipo/rk3399-pinebook-pro.dtb";
  };
in
{
  inherit modules;

  quartz64a = buildSystem modules.quartz64a;
  quartz64b = buildSystem modules.quartz64b;
  soquartz-model-a = buildSystem modules.soquartz-model-a;
  soquartz-cm4 = buildSystem modules.rk3566-soquartz-cm4;
  soquartz-blade = buildSystem modules.soquartz-blade;
  rock64 = buildSystem modules.rock64;
  rockPro64 = buildSystem modules.rockPro64;
  rockPro64v2 = buildSystem modules.rockPro64v2;
  roc-pc-rk3399 = buildSystem modules.roc-pc-rk3399;
  pinebookPro = buildSystem modules.pinebookPro;
}
