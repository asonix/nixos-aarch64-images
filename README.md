# NixOS aarch64 images

Build aarch64 images for ARM single board computer that require
custom uboot firmware.

Based on original work here: [https://github.com/Mic92/nixos-aarch64-images](https://github.com/Mic92/nixos-aarch64-images)

## Example (requires flakes)

```console
$ nix build .#soquartz-blade
$ nix build .#soquartz-cm4
$ nix build .#quartz64b
```

Built images can be than copied to sdcards etc as usual:

``` console
$ sudo dd if=./result of=/dev/mmcblk0 iflag=direct oflag=direct bs=16M status=progress
```

Replace `/dev/mmcblk0` with your actual device.

Of course, you can replace `soquartz-blade` with any of the outputs included in this flake. To see the outputs, you can
invoke `nix flake show`:

```
git+file:///home/asonix/Development/nix/nixos-aarch64-images
└───packages
    └───x86_64-linux
        ├───pinebookPro: package 'image'
        ├───quartz64a: package 'image'
        ├───quartz64b: package 'image'
        ├───roc-pc-rk3399: package 'image'
        ├───rock64: package 'image'
        ├───rockPro64: package 'image'
        ├───rockPro64v2: package 'image'
        ├───soquartz-blade: package 'image'
        ├───soquartz-cm4: package 'image'
        └───soquartz-model-a: package 'image'
```


## Supported boards

| Board                               | Attribute        | Status                  |
| ------------------------------------|------------------| ------------------------|
| [SoQuartz with Blade Baseboard][]   | soquartz-blade   | Tested & works          |
| [SoQuartz with CM4IO Baseboard][]   | soquartz-cm4     | Untested                |
| [SoQuartz with Model A Baseboard][] | soquartz-model-a | Untested                |
| [Quartz64 Model A][]                | quartz64a        | Tested & works          |
| [Quartz64 Model B][]                | quartz64b        | Tested & works          |
| [Rock64][]                          | rock64           | Tested & works          |
| [RockPro64][]                       | rockPro64        | Untested since I forked |
| [RockPro64 (v2)][]                  | rockPro64v2      | Tested & works          |
| [roc-pc-rk3399][]                   | roc-pc-rk3399    | Untested since I forked |
| [PinebookPro][]                     | pinebookPro      | Untested since I forked |

[SoQuartz with Blade Baseboard]: https://wiki.pine64.org/wiki/SOQuartz#SOQuartz_BLADE_Baseboard_Features
[SoQuartz with CM4IO Baseboard]: https://wiki.pine64.org/wiki/SOQuartz#SOQuartz_Module_with_various_CM4_carrier_boards
[SoQuartz with Model A Baseboard]: https://wiki.pine64.org/wiki/SOQuartz#SOQuartz_Model-A_Baseboard_Features
[Quartz64 Model A]: https://wiki.pine64.org/wiki/Quartz64
[Quartz64 Model B]: https://wiki.pine64.org/wiki/Quartz64
[Rock64]: https://nixos.wiki/wiki/NixOS_on_ARM/PINE64_ROCK64
[RockPro64]: https://nixos.wiki/wiki/NixOS_on_ARM/PINE64_ROCKPro64
[RockPro64 (v2)]: https://nixos.wiki/wiki/NixOS_on_ARM/PINE64_ROCKPro64
[roc-pc-rk3399]: https://nixos.wiki/wiki/NixOS_on_ARM/Libre_Computer_ROC-RK3399-PC
[PinebookPro]: https://nixos.wiki/wiki/NixOS_on_ARM/PINE64_Pinebook_Pro

## Add a new board

See `images/rockchip.nix` for an example.
All options are defined in [here](pkgs/build-image/options.nix);

## Board wishlist

Allwinner boards have their bootloader in a free space after mbr:

https://nixos.wiki/wiki/Template:ARM/installation_allwinner
