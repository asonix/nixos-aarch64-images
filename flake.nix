{
  description = "Build NixOS images for various ARM single computer boards";

  # pin this to unstable
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config = { allowUnfree = true; };
      };

      aarch64Pkgs = import pkgs.path {
        system = "aarch64-linux";
        config = { allowUnfree = true; };
      };

      rockchip = { system, uboot }: pkgs.callPackage ./images/rockchip.nix {
        inherit uboot;

        buildImage = pkgs.callPackage ./pkgs/build-image { };
        image = pkgs.callPackage ./pkgs/aarch64-image {
          inherit system;
        };
      };

      kernels = (aarch64Pkgs.callPackage ./pkgs/linux { });

      buildSystem = ({ kernel, modules }: aarch64Pkgs.callPackage ./pkgs/system {
        inherit kernel nixpkgs;

        extraModules = modules;
      });

      quartzUBoots = aarch64Pkgs.callPackage ./pkgs/uboot-quartz64 { };
    in
    {
      packages.x86_64-linux = {
        inherit kernels;

        buildModules = ({ kernel, modules ? [ ] }@attrs: (buildSystem attrs).modules);

        buildQuartz64A = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.quartz64a;
            uboot = quartzUBoots.ubootQuartz64a;
          });

        buildQuartz64B = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.quartz64b;
            uboot = quartzUBoots.ubootQuartz64b;
          });

        buildSoQuartzModelA = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.soquartz-model-a;
            uboot = quartzUBoots.ubootSoQuartz;
          });

        buildSoQuartzCM4 = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.soquartz-cm4;
            uboot = quartzUBoots.ubootSoQuartz;
          });

        buildSoQuartzBlade = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.soquartz-blade;
            uboot = quartzUBoots.ubootSoQuartz;
          });

        buildRock64 = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.rock64;
            uboot = aarch64Pkgs.ubootRock64;
          });

        buildRockPro64 = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.rockPro64;
            uboot = aarch64Pkgs.ubootRockPro64;
          });

        buildRockPro64v2 = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.rockPro64v2;
            uboot = aarch64Pkgs.ubootRockPro64;
          });

        buildRocPCRk3399 = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.rock-pc-rk3399;
            uboot = aarch64Pkgs.ubootROCPCRK3399;
          });

        buildPinebookPro = ({ kernel, modules ? [ ] }@attrs:
          let system = buildSystem attrs; in
          rockchip {
            system = system.pinebookPro;
            uboot = aarch64Pkgs.ubootPinebookPro;
          });
      };
    };
}
