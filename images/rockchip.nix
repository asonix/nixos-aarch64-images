{ lib
, buildImage
, uboot
, image
, extraConfig ? { }
}:

let
  idbloaderOffset = 64; # 0x40
  ubootOffset = 16384; # 0x4000
  rootOffset = 65536;
in
buildImage {
  config = {
    imports = [ extraConfig ];
    format = "gpt";
    partitions = {
      idbloader = {
        source = "${uboot}/idbloader.img";
        size = ubootOffset - idbloaderOffset;
        start = idbloaderOffset;
      };
      uboot = {
        source = "${uboot}/u-boot.itb";
        size = rootOffset - ubootOffset;
        start = ubootOffset;
      };
      nixos = {
        source = image;
        start = rootOffset;
        attrs = "LegacyBIOSBootable";
        type = "b921b045-1df0-41c3-af44-4c6f280d3fae";
        useBootPartition = true;
      };
    };
  };
}
